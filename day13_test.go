package main

import (
	"strconv"
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_getNextTime(t *testing.T) {
	res := getNextTime(939, 7)
	require.Equal(t, 945, res)
}

func Test_getNextTime2(t *testing.T) {
	res := getNextTime(939, 59)
	require.Equal(t, 944, res)
}

func Test_getNextTimeFromBuses(t *testing.T) {
	time, bus := getNextTimeFromBuses(939, []int{7, 13, 59, 31, 19})
	require.Equal(t, 944, time)
	require.Equal(t, 59, bus)
}

func Test_getBusesFromString(t *testing.T) {
	res := getBusesFromString("7,13,x,x,59,x,31,19")
	require.Equal(t, []int{7, 13, 59, 31, 19}, res)
}

func Test_puzzle1(t *testing.T) {
	input, err := fileToStringSlice("inputs/day13.txt")
	require.Nil(t, err)
	time, err2 := strconv.Atoi(input[0])
	require.Nil(t, err2)
	buses := getBusesFromString(input[1])
	nextTime, bus := getNextTimeFromBuses(time, buses)
	answer := bus * (nextTime - time)
	require.Equal(t, 4135, answer)
}
