package main

import (
	"errors"
)

var errNotFound = errors.New("not found")

func findPairThatSumTo2020(s []int) (int, int, error) {
	for _, i := range s {
		for _, j := range s {
			if i+j == 2020 {
				return i, j, nil
			}
		}
	}
	return 0, 0, errNotFound
}

func findProductOfPairThatSumTo2020(s []int) (int, error) {
	p1, p2, err := findPairThatSumTo2020(s)
	return p1 * p2, err
}

func findTripletThatSumTo2020(s []int) (int, int, int, error) {
	for _, i := range s {
		for _, j := range s {
			for _, k := range s {
				if i+j+k == 2020 {
					return i, j, k, nil
				}
			}
		}
	}
	return 0, 0, 0, errNotFound
}

func findProductOfTripletThatSumTo2020(s []int) (int, error) {
	p1, p2, p3, err := findTripletThatSumTo2020(s)
	return p1 * p2 * p3, err
}
