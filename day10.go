package main

import "sort"

func find1Jolt3JoltDiffProduct(s []int) int {
	sort.Ints(s)
	var ones, threes int
	prev := 0
	for _, i := range s {
		if i-prev == 1 {
			ones++
		}
		if i-prev == 3 {
			threes++
		}
		prev = i
	}
	threes++
	return ones * threes
}
