package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_findPairThatSumTo2020(t *testing.T) {
	s := []int{1721, 979, 366, 299, 675, 1456}
	p1, p2, err := findPairThatSumTo2020(s)
	require.Nil(t, err)
	require.Equal(t, 1721, p1)
	require.Equal(t, 299, p2)
}

func Test_findPairThatSumTo2020_NotFound(t *testing.T) {
	s := []int{100, 200, 300, 400}
	p1, p2, err := findPairThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, p1)
	require.Equal(t, 0, p2)
}

func Test_findPairThatSumTo2020_EmptyInput(t *testing.T) {
	s := []int{}
	p1, p2, err := findPairThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, p1)
	require.Equal(t, 0, p2)
}
func Test_findProductOfPairThatSumTo2020(t *testing.T) {
	s := []int{1721, 979, 366, 299, 675, 1456}
	res, err := findProductOfPairThatSumTo2020(s)
	require.Nil(t, err)
	require.Equal(t, 514579, res)
}

func Test_findProductOfPairThatSumTo2020_NotFound(t *testing.T) {
	s := []int{100, 200, 300, 400}
	res, err := findProductOfPairThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, res)
}

func Test_findProductOfPairThatSumTo2020_EmptyInput(t *testing.T) {
	s := []int{}
	res, err := findProductOfPairThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, res)
}

func Test_findProductOfPairThatSumTo2020_puzzle1(t *testing.T) {
	s, fileErr := fileToIntSlice("inputs/day1.txt")
	require.Nil(t, fileErr)
	res, err := findProductOfPairThatSumTo2020(s)
	require.Nil(t, err)
	require.Equal(t, 898299, res)
}

func Test_findTripletThatSumTo2020(t *testing.T) {
	s := []int{1721, 979, 366, 299, 675, 1456}
	p1, p2, p3, err := findTripletThatSumTo2020(s)
	require.Nil(t, err)
	require.Equal(t, 979, p1)
	require.Equal(t, 366, p2)
	require.Equal(t, 675, p3)
}

func Test_findTripletThatSumTo2020_NotFound(t *testing.T) {
	s := []int{100, 200, 300, 400}
	p1, p2, p3, err := findTripletThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, p1)
	require.Equal(t, 0, p2)
	require.Equal(t, 0, p3)
}

func Test_findTripletThatSumTo2020_EmptyInput(t *testing.T) {
	s := []int{}
	p1, p2, p3, err := findTripletThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, p1)
	require.Equal(t, 0, p2)
	require.Equal(t, 0, p3)
}

func Test_findProductOfTripletThatSumTo2020(t *testing.T) {
	s := []int{1721, 979, 366, 299, 675, 1456}
	res, err := findProductOfTripletThatSumTo2020(s)
	require.Nil(t, err)
	require.Equal(t, 241861950, res)
}

func Test_findProductOfTripletThatSumTo2020_NotFound(t *testing.T) {
	s := []int{100, 200, 300, 400}
	res, err := findProductOfTripletThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, res)
}

func Test_findProductOfTripletThatSumTo2020_EmptyInput(t *testing.T) {
	s := []int{}
	res, err := findProductOfTripletThatSumTo2020(s)
	require.Equal(t, errNotFound, err)
	require.Equal(t, 0, res)
}

func Test_findProductOfPairThatSumTo2020_puzzle2(t *testing.T) {
	s, fileErr := fileToIntSlice("inputs/day1.txt")
	require.Nil(t, fileErr)
	res, err := findProductOfTripletThatSumTo2020(s)
	require.Nil(t, err)
	require.Equal(t, 143933922, res)
}
