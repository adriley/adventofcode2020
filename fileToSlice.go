package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func convertToIntSlice(s []string) ([]int, error) {
	ret := make([]int, len(s))
	for i := 0; i < len(s); i++ {
		j, err := strconv.Atoi(s[i])
		if err != nil {
			return nil, fmt.Errorf("error converting element %v: %v", i, err)
		}
		ret[i] = j
	}
	return ret, nil
}

func fileToStringSlice(fileName string) ([]string, error) {
	fileBytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	fileString := string(fileBytes)
	lineDelimiter := "\n"
	if strings.Contains(fileString, "\r\n") {
		lineDelimiter = "\r\n"
	}
	stringSlice := strings.Split(fileString, lineDelimiter)
	return stringSlice, nil
}

func fileToIntSlice(fileName string) ([]int, error) {
	stringSlice, err := fileToStringSlice(fileName)
	if err != nil {
		return nil, err
	}
	ret, err := convertToIntSlice(stringSlice)
	return ret, err
}
