package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_convertToIntSlice(t *testing.T) {
	res, err := convertToIntSlice([]string{"1", "2", "76"})
	require.Nil(t, err)
	require.Equal(t, []int{1, 2, 76}, res)
}

func Test_convertToIntSlice_Error(t *testing.T) {
	res, err := convertToIntSlice([]string{"1", "ghost", "76"})
	require.Equal(t, `error converting element 1: strconv.Atoi: parsing "ghost": invalid syntax`, err.Error())
	require.Nil(t, res)
}

func Test_fileToIntSlice_BadFileName(t *testing.T) {
	s, err := fileToIntSlice("badfilename")
	require.Equal(t, "open badfilename: The system cannot find the file specified.", err.Error())
	require.Nil(t, s)
}
