package main

import (
	"fmt"
	"strconv"
	"strings"
)

func parsePassports(raw []string) []string {
	res := []string{}
	passport := ""
	for _, v := range raw {
		if v == "" {
			res = append(res, passport)
			passport = ""
		} else {
			if passport != "" {
				passport += " "
			}
			passport += v
		}
	}
	res = append(res, passport)
	return res
}

func validPassport(s string) bool {
	return strings.Contains(s, "byr:") && strings.Contains(s, "iyr:") && strings.Contains(s, "eyr:") && strings.Contains(s, "hgt:") && strings.Contains(s, "hcl:") && strings.Contains(s, "ecl:") && strings.Contains(s, "pid:") //&& strings.Contains(s, "cid:")
}

func countValidPassports(passports []string) int {
	res := 0
	for _, p := range passports {
		if validPassport(p) {
			res++
		}
	}
	return res
}

func countValidPassports2(passports []string) int {
	res := 0
	for _, p := range passports {
		if validPassport2(p) {
			res++
		}
	}
	return res
}

func validPassport2(s string) bool {
	if !validPassport(s) {
		return false
	}
	fields := strings.Split(s, " ")
	for _, f := range fields {
		field := strings.Split(f, ":")
		if !fieldValid(field[0], field[1]) {
			return false
		}
	}
	return true
}

func byrFieldValid(value string) bool {
	n, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return n >= 1920 && n <= 2002
}

func iyrFieldValid(value string) bool {
	n, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return n >= 2010 && n <= 2020
}

func eyrFieldValid(value string) bool {
	n, err := strconv.Atoi(value)
	if err != nil {
		return false
	}
	return n >= 2020 && n <= 2030
}

func hgtFieldValid(value string) bool {
	n := 0
	_, err := fmt.Sscanf(value, "%dcm", &n)
	if err == nil {
		return n >= 150 && n <= 193
	}
	_, err = fmt.Sscanf(value, "%din", &n)
	if err == nil {
		return n >= 59 && n <= 76
	}
	return false
}

func hclFieldValid(value string) bool {
	if len(value) != 7 {
		return false
	}
	if value[0] != '#' {
		return false
	}
	for i := 1; i <= 6; i++ {
		if !((value[i] >= '0' && value[i] <= '9') || (value[i] >= 'a' && value[i] <= 'f')) {
			return false
		}
	}
	return true
}

func eclFieldValid(value string) bool {
	switch value {
	case "amb", "blu", "brn", "gry", "grn", "hzl", "oth":
		return true
	}
	return false
}

func pidFieldValid(value string) bool {
	if len(value) != 9 {
		return false
	}
	for _, c := range value {
		if c < '0' || c > '9' {
			return false
		}
	}
	return true
}

func cidFieldValid(value string) bool {
	return true
}

func fieldValid(fieldType string, fieldValue string) bool {
	switch fieldType {
	case "byr":
		return byrFieldValid(fieldValue)
	case "iyr":
		return iyrFieldValid(fieldValue)
	case "eyr":
		return eyrFieldValid(fieldValue)
	case "hgt":
		return hgtFieldValid(fieldValue)
	case "hcl":
		return hclFieldValid(fieldValue)
	case "ecl":
		return eclFieldValid(fieldValue)
	case "pid":
		return pidFieldValid(fieldValue)
	case "cid":
		return cidFieldValid(fieldValue)
	}
	return false
}
