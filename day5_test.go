package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_calculateRow(t *testing.T) {
	res := calculateRow("FBFBBFFRLR")
	require.Equal(t, 44, res)
}

func Test_calculateColumn(t *testing.T) {
	res := calculateColumn("FBFBBFFRLR")
	require.Equal(t, 5, res)
}

func Test_calculateSeatId(t *testing.T) {
	res := calculateSeatId("FBFBBFFRLR")
	require.Equal(t, 357, res)
}

func Test_calculateHighestSeatSeatId(t *testing.T) {
	input := []string{"FBFBBFFRLR", "FFFBBBFRRR", "BBFFBBFRLL"}
	res := calculateHighestSeatSeatId(input)
	require.Equal(t, 820, res)
}

func Test_calculateHighestSeatSeatId_Puzzle1(t *testing.T) {
	input, err := fileToStringSlice("inputs/day5.txt")
	require.Nil(t, err)
	res := calculateHighestSeatSeatId(input)
	require.Equal(t, 866, res)
}

func Test_FindSeatId_Puzzle2(t *testing.T) {
	input, err := fileToStringSlice("inputs/day5.txt")
	require.Nil(t, err)
	res := findSeatId(input)
	require.Equal(t, 583, res)
}
