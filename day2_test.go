package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_countOccurances_happyPath(t *testing.T) {
	res := countOccurances("abcde", 'a')
	require.Equal(t, 1, res)
}

func Test_countOccurances_emptystring(t *testing.T) {
	res := countOccurances("", 'a')
	require.Equal(t, 0, res)
}

func Test_parseDay2InputLine_Sample1(t *testing.T) {
	lower, upper, char, password, err := parseDay2InputLine("1-3 a: abcde")
	require.Nil(t, err)
	require.Equal(t, 1, lower)
	require.Equal(t, 3, upper)
	require.Equal(t, 'a', char)
	require.Equal(t, "abcde", password)
}

func Test_parseDay2InputLine_Sample2(t *testing.T) {
	lower, upper, char, password, err := parseDay2InputLine("2-9 c: ccccccccc")
	require.Nil(t, err)
	require.Equal(t, 2, lower)
	require.Equal(t, 9, upper)
	require.Equal(t, 'c', char)
	require.Equal(t, "ccccccccc", password)
}

func Test_parseDay2InputLine_BadInput(t *testing.T) {
	lower, upper, char, password, err := parseDay2InputLine("Bad Input")
	require.Equal(t, `Input does not match Sscanf pattern "%d-%d %c: %s" with message: expected integer`, err.Error())
	require.Equal(t, 0, lower)
	require.Equal(t, 0, upper)
	require.Equal(t, rune(0), char)
	require.Equal(t, "", password)
}

func Test_parseDay2InputLine_HalfBadInput(t *testing.T) {
	lower, upper, char, password, err := parseDay2InputLine("1-2 Half Bad Input")
	require.Equal(t, `Input does not match Sscanf pattern "%d-%d %c: %s" with message: input does not match format`, err.Error())
	require.Equal(t, 1, lower)
	require.Equal(t, 2, upper)
	require.Equal(t, 'H', char)
	require.Equal(t, "", password)
}

func Test_validPassword_TrueLowerLimit(t *testing.T) {
	res := validPassword(1, 3, 'a', "abcde")
	require.Equal(t, res, true)
}

func Test_validPassword_TrueUpperLimit(t *testing.T) {
	res := validPassword(1, 3, 'a', "abcadea")
	require.Equal(t, res, true)
}

func Test_validPassword_FalseLowerLimit(t *testing.T) {
	res := validPassword(1, 3, 'a', "bcde")
	require.Equal(t, res, false)
}

func Test_validPassword_FalseUpperLimit(t *testing.T) {
	res := validPassword(1, 3, 'c', "bcdeccc")
	require.Equal(t, res, false)
}

func Test_countValidPasswords(t *testing.T) {
	res := countValidPasswords([]string{"1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"})
	require.Equal(t, res, 2)
}

func Test_countValidPasswords_puzzle1(t *testing.T) {
	passwords, err := fileToStringSlice("inputs/day2.txt")
	require.Nil(t, err)
	res := countValidPasswords(passwords)
	require.Equal(t, 483, res)
}

func Test_validPasswordPart2_True(t *testing.T) {
	res := validPasswordPart2(1, 3, 'a', "abcde")
	require.Equal(t, res, true)
}

func Test_validPasswordPart2_False1(t *testing.T) {
	res := validPasswordPart2(1, 3, 'b', "cdefg")
	require.Equal(t, res, false)
}

func Test_validPasswordPart2_False2(t *testing.T) {
	res := validPasswordPart2(2, 9, 'c', "ccccccccc")
	require.Equal(t, res, false)
}

func Test_countValidPasswords_puzzle2(t *testing.T) {
	passwords, err := fileToStringSlice("inputs/day2.txt")
	require.Nil(t, err)
	res := countValidPasswordsPart2(passwords)
	require.Equal(t, 482, res)
}
