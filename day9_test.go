package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_calcValidNumbers(t *testing.T) {
	res := calcValidNumbers([]int{35, 20, 15})
	require.Equal(t, map[int]bool{35: true, 50: true, 55: true}, res)
}

func Test_findFirstInvalidNumber(t *testing.T) {
	input := []int{35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576}
	res := findFirstInvalidNumber(input, 5)
	require.Equal(t, 127, res)
}

func Test_findFirstInvalidNumber_puzzle1(t *testing.T) {
	input, err := fileToIntSlice("inputs/day9.txt")
	require.Nil(t, err)
	res := findFirstInvalidNumber(input, 25)
	require.Equal(t, 400480901, res)
}

func Test_findContiguosNumber(t *testing.T) {
	input := []int{1, 2, 3, 4, 5}
	res := findContiguosNumber(input, 5)
	require.Equal(t, 5, res)
}

func Test_findContiguosNumber2(t *testing.T) {
	input := []int{35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576}
	res := findContiguosNumber(input, 127)
	require.Equal(t, 62, res)
}

func Test_findContiguosNumber_puzzle2(t *testing.T) {
	input, err := fileToIntSlice("inputs/day9.txt")
	require.Nil(t, err)
	res := findContiguosNumber(input, 400480901)
	require.Equal(t, 67587168, res)
}
