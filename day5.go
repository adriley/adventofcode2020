package main

import "sort"

func calculateRow(s string) int {
	l, u := 0, 127
	for _, c := range s {
		if c == 'F' {
			u = u - (u-l+1)/2
		}
		if c == 'B' {
			l = l + (u-l+1)/2
		}
	}
	return l
}

func calculateColumn(s string) int {
	l, u := 0, 7
	for _, c := range s {
		if c == 'L' {
			u = u - (u-l+1)/2
		}
		if c == 'R' {
			l = l + (u-l+1)/2
		}
	}
	return l
}

func calculateSeatId(s string) int {
	return calculateRow(s)*8 + calculateColumn(s)
}

func calculateHighestSeatSeatId(slice []string) int {
	res := 0
	for _, s := range slice {
		seatId := calculateSeatId(s)
		if seatId > res {
			res = seatId
		}
	}
	return res
}

func findSeatId(slice []string) int {
	seatIds := []int{}
	for _, s := range slice {
		seatId := calculateSeatId(s)
		seatIds = append(seatIds, seatId)
	}
	sort.Ints(seatIds)
	for i := 1; i <= len(seatIds); i++ {
		if seatIds[i]-1 != seatIds[i-1] {
			return seatIds[i] - 1
		}
	}
	return 0
}
