package main

import (
	"fmt"
	"strings"
)

type computer struct {
	program         []programInstruction
	accumulator     int
	nextInstruction int
}

func (c *computer) Run() (bool, int) {
	for c.nextInstruction < len(c.program) {
		i := c.program[c.nextInstruction]
		if i.ran {
			return false, c.accumulator
		}
		i.ran = true
		c.program[c.nextInstruction] = i
		i.i.run(c)
	}
	return true, c.accumulator
}

type programInstruction struct {
	i   instruction
	ran bool
}

type instruction interface {
	run(c *computer)
}

type nop struct {
}

func (i nop) run(c *computer) {
	c.nextInstruction++
}

type acc struct {
	value int
}

func (i acc) run(c *computer) {
	c.accumulator += i.value
	c.nextInstruction++
}

type jmp struct {
	offset int
}

func (i jmp) run(c *computer) {
	c.nextInstruction += i.offset
}

func parseInstruction(s string) instruction {
	var operation string
	var value int
	fmt.Sscanf(s, "%s %d", &operation, &value)
	switch operation {
	case "nop":
		return nop{}
	case "jmp":
		return jmp{value}
	case "acc":
		return acc{value}
	}
	return nop{}
}

func parseInstructions(input []string) []programInstruction {
	res := []programInstruction{}
	for _, s := range input {
		res = append(res, programInstruction{parseInstruction(s), false})
	}
	return res
}

func modifyRawInput(input []string, i int) (bool, []string) {
	res := make([]string, len(input))
	copy(res, input)
	s := res[i]
	if strings.Contains(s, "nop") {
		res[i] = strings.ReplaceAll(s, "nop", "jmp")
		return true, res
	}
	if strings.Contains(s, "jmp") {
		res[i] = strings.ReplaceAll(s, "jmp", "nop")
		return true, res
	}
	return false, res
}

func modifyAndRunProgramUntilSuccess(input []string) int {
	var res bool
	var acc int
	for i := 0; i < len(input); i++ {
		modified, modifiedInput := modifyRawInput(input, i)
		if !modified {
			continue
		}
		pi := parseInstructions(modifiedInput)
		c := computer{pi, 0, 0}
		res, acc = c.Run()
		if res {
			break
		}
	}
	return acc
}
