package main

import (
	"regexp"
	"strconv"
)

type child struct {
	color string
	count int
}

type rule struct {
	parentColor string
	children    []child
}

var regexParentChildren = regexp.MustCompile(`(.*) bags contain (.*)`)
var regexChild = regexp.MustCompile(`(\d+) (.*?) bag`)

func parseRule(s string) rule {
	res := rule{"", []child{}}
	match := regexParentChildren.FindStringSubmatch(s)
	res.parentColor = match[1]
	childrenRaw := match[2]
	children := regexChild.FindAllStringSubmatch(childrenRaw, -1)
	for _, childMatch := range children {
		childCount, _ := strconv.Atoi(childMatch[1])
		childColor := childMatch[2]
		c := child{childColor, childCount}
		res.children = append(res.children, c)
	}
	return res
}

func parseRules(s []string) []rule {
	res := []rule{}
	for _, r := range s {
		res = append(res, parseRule(r))
	}
	return res
}

func countParentBagColors(s string, rules []rule) int {
	parentBagColors := make(map[string]bool)
	getParentBagColors(s, rules, parentBagColors)
	return len(parentBagColors)
}

func getParentBagColors(s string, rules []rule, parentBagColors map[string]bool) {
	for _, r := range rules {
		for _, c := range r.children {
			if c.color == s {
				if !parentBagColors[r.parentColor] {
					parentBagColors[r.parentColor] = true
					getParentBagColors(r.parentColor, rules, parentBagColors)
				}
			}
		}
	}
}

func countChildBags(s string, rules []rule) int {
	res := 0
	for _, r := range rules {
		if r.parentColor == s {
			for _, c := range r.children {
				res += c.count * countChildBags(c.color, rules)
			}
			res++
		}
	}
	return res
}
