package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_day8_puzzle1(t *testing.T) {
	input, err := fileToStringSlice("inputs/day8.txt")
	require.Nil(t, err)
	pi := parseInstructions(input)
	c := computer{pi, 0, 0}
	res, acc := c.Run()
	require.False(t, res)
	require.Equal(t, 2034, acc)
}

func Test_day8_puzzle2(t *testing.T) {
	input, err := fileToStringSlice("inputs/day8.txt")
	require.Nil(t, err)
	res := modifyAndRunProgramUntilSuccess(input)
	require.Equal(t, 672, res)
}
