package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_countYesAnyoneQuestions(t *testing.T) {
	res := countAnyoneYesQuestions("ab ac")
	require.Equal(t, 3, res)
}

func Test_countYesEveryoneoneQuestions(t *testing.T) {
	res := countEveryoneYesQuestions("ab ac")
	require.Equal(t, 1, res)
}

func Test_sumYesQuestions(t *testing.T) {
	raw, err := fileToStringSlice("inputs/day6.txt")
	require.Nil(t, err)
	answers := parsePassports(raw)
	res := sumYesQuestions(answers)
	require.Equal(t, 7128, res)
}

func Test_sumEveryoneYesQuestions(t *testing.T) {
	raw, err := fileToStringSlice("inputs/day6.txt")
	require.Nil(t, err)
	answers := parsePassports(raw)
	res := sumEveryoneYesQuestions(answers)
	require.Equal(t, 3640, res)
}
