package main

import (
	"math/bits"
	"strings"
)

func countAnyoneYesQuestions(s string) int {
	questions := make(map[rune]int)
	for _, r := range s {
		if r != ' ' {
			questions[r] = questions[r] + 1
		}
	}
	return len(questions)
}

func setBit(n uint32, pos uint) uint32 {
	n |= (1 << pos)
	return n
}

func getBitMask(s string) uint32 {
	res := uint32(0)
	for _, r := range s {
		res = setBit(res, uint(r)-'a')
	}
	return res
}

func countEveryoneYesQuestions(s string) int {
	p := strings.Split(s, " ")
	mask := getBitMask(p[0])
	for i := 1; i < len(p); i++ {
		mask = mask & getBitMask(p[i])
	}
	return bits.OnesCount32(mask)
}

func sumYesQuestions(answers []string) int {
	res := 0
	for _, s := range answers {
		res += countAnyoneYesQuestions(s)
	}
	return res
}

func sumEveryoneYesQuestions(answers []string) int {
	res := 0
	for _, s := range answers {
		res += countEveryoneYesQuestions(s)
	}
	return res
}
