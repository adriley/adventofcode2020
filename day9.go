package main

func calcValidNumbers(input []int) map[int]bool {
	res := make(map[int]bool)
	for i, a := range input {
		for j, b := range input {
			if i != j {
				res[a+b] = true
			}
		}
	}
	return res
}

func findFirstInvalidNumber(input []int, pl int) int {
	for i := pl; i < len(input); i++ {
		if calcValidNumbers(input[i-pl : i])[input[i]] == false {
			return input[i]
		}
	}
	return 0
}

func findContiguosNumber(input []int, answer int) int {
	var min, max int
	for i := 0; i < len(input); i++ {
		sum := input[i]
		min = input[i]
		max = input[i]
		for j := i + 1; j < len(input); j++ {
			sum += input[j]
			if input[j] < min {
				min = input[j]
			}
			if input[j] > max {
				max = input[j]
			}
			if sum == answer {
				return min + max
			}
		}
	}
	return 0
}
