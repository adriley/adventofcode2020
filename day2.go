package main

import (
	"fmt"
)

func countOccurances(s string, r rune) int {
	res := 0
	for _, c := range s {
		if c == r {
			res++
		}
	}
	return res
}

func parseDay2InputLine(s string) (lower, upper int, char rune, password string, err error) {
	_, err = fmt.Sscanf(s, "%d-%d %c: %s", &lower, &upper, &char, &password)
	if err != nil {
		err = fmt.Errorf(`Input does not match Sscanf pattern "%%d-%%d %%c: %%s" with message: %w`, err)
	}
	return
}

func validPassword(lower, upper int, char rune, password string) bool {
	count := countOccurances(password, char)
	return count >= lower && count <= upper
}

func countValidPasswords(passwords []string) int {
	res := 0
	for _, password := range passwords {
		lower, upper, char, password, err := parseDay2InputLine(password)
		if err == nil && validPassword(lower, upper, char, password) {
			res++
		}
	}
	return res
}

func validPasswordPart2(pos1, pos2 int, char rune, password string) bool {
	count := 0
	if password[pos1-1] == byte(char) {
		count++
	}
	if password[pos2-1] == byte(char) {
		count++
	}
	return count == 1
}

func countValidPasswordsPart2(passwords []string) int {
	res := 0
	for _, password := range passwords {
		pos1, pos2, char, password, err := parseDay2InputLine(password)
		if err == nil && validPasswordPart2(pos1, pos2, char, password) {
			res++
		}
	}
	return res
}
