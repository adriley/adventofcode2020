package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_treeCounter_Simple1(t *testing.T) {
	grid := make([]string, 5)
	grid[0] = "..#.."
	grid[1] = "..#.."
	grid[2] = ".#..."
	grid[3] = "..##."
	grid[4] = ".##.."
	res := treeCounter(grid, 3, 1)
	require.Equal(t, 2, res)
}

func Test_treeCounter_WideStride(t *testing.T) {
	grid := make([]string, 5)
	grid[0] = "..#.."
	grid[1] = "..#.."
	grid[2] = ".#..."
	grid[3] = "..##."
	grid[4] = ".##.."
	res := treeCounter(grid, 13, 1)
	require.Equal(t, 2, res)
}

func Test_treeCounter_Puzzle1(t *testing.T) {
	grid, err := fileToStringSlice("inputs/day3.txt")
	require.Nil(t, err)
	res := treeCounter(grid, 3, 1)
	require.Equal(t, 220, res)
}

func Test_treeCounter_Puzzle2(t *testing.T) {
	grid, err := fileToStringSlice("inputs/day3.txt")
	require.Nil(t, err)
	res := treeCounter(grid, 1, 1) * treeCounter(grid, 3, 1) * treeCounter(grid, 5, 1) * treeCounter(grid, 7, 1) * treeCounter(grid, 1, 2)
	require.Equal(t, 2138320800, res)
}
