package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_parseRule_NoChildren(t *testing.T) {
	res := parseRule("dotted black bags contain no other bags.")
	require.Equal(t, "dotted black", res.parentColor)
	require.Equal(t, 0, len(res.children))
}

func Test_parseRule_TwoChildren(t *testing.T) {
	res := parseRule("dark olive bags contain 3 faded blue bags, 4 dotted black bags.")
	require.Equal(t, "dark olive", res.parentColor)
	require.Equal(t, 2, len(res.children))
	require.Equal(t, 3, res.children[0].count)
	require.Equal(t, "faded blue", res.children[0].color)
	require.Equal(t, 4, res.children[1].count)
	require.Equal(t, "dotted black", res.children[1].color)
}

func Test_countParentBagColors(t *testing.T) {
	testInput := []string{"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
		"bright white bags contain 1 shiny gold bag.",
		"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
		"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
		"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
		"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
		"faded blue bags contain no other bags.",
		"dotted black bags contain no other bags."}
	rules := parseRules(testInput)
	res := countParentBagColors("shiny gold", rules)
	require.Equal(t, 4, res)
}

func Test_countParentBagColors_puzzle1(t *testing.T) {
	input, err := fileToStringSlice("inputs/day7.txt")
	require.Nil(t, err)
	rules := parseRules(input)
	res := countParentBagColors("shiny gold", rules)
	require.Equal(t, 124, res)
}

func Test_countChildBags(t *testing.T) {
	testInput := []string{"light red bags contain 1 bright white bag, 2 muted yellow bags.",
		"dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
		"bright white bags contain 1 shiny gold bag.",
		"muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
		"shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
		"dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
		"vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
		"faded blue bags contain no other bags.",
		"dotted black bags contain no other bags."}
	rules := parseRules(testInput)
	res := countChildBags("shiny gold", rules)
	require.Equal(t, 32, res-1)
}

func Test_countChildBags_puzzle2(t *testing.T) {
	input, err := fileToStringSlice("inputs/day7.txt")
	require.Nil(t, err)
	rules := parseRules(input)
	res := countChildBags("shiny gold", rules)
	require.Equal(t, 34862, res-1)
}
