package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_find1Jolt3JoltDiffProduct(t *testing.T) {
	s := []int{16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4}
	res := find1Jolt3JoltDiffProduct(s)
	require.Equal(t, 35, res)
}

func Test_find1Jolt3JoltDiffProduct_puzzle1(t *testing.T) {
	s, fileErr := fileToIntSlice("inputs/day10.txt")
	require.Nil(t, fileErr)
	res := find1Jolt3JoltDiffProduct(s)
	require.Equal(t, 2170, res)
}
