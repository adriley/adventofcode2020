package main

func treeCounter(grid []string, right, down int) int {
	res := 0
	x := 0
	for y := 0; y < len(grid); y += down {
		row := grid[y]
		if row[x] == '#' {
			res++
		}
		x = (x + right) % len(row)
	}
	return res
}
