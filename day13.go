package main

import (
	"strconv"
	"strings"
)

func getNextTime(t int, b int) int {
	lastBus := (t / b) * b
	nextBus := lastBus + b
	return nextBus
}

func getNextTimeFromBuses(t int, buses []int) (time int, bus int) {
	for _, b := range buses {
		nextBus := getNextTime(t, b)
		if time == 0 || nextBus < time {
			time = nextBus
			bus = b
		}
	}
	return time, bus
}

func getBusesFromString(s string) []int {
	ret := []int{}
	slice := strings.Split(s, ",")
	for _, i := range slice {
		j, err := strconv.Atoi(i)
		if err == nil {
			ret = append(ret, j)
		}
	}
	return ret
}
