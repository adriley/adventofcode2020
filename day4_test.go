package main

import (
	"testing"

	"gitlab.com/adriley/qcic/require"
)

func Test_parsePassports(t *testing.T) {
	input := []string{"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd",
		"byr:1937 iyr:2017 cid:147 hgt:183cm",
		"",
		"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884",
		"hcl:#cfa07d byr:1929",
		"",
		"hcl:#ae17e1 iyr:2013",
		"eyr:2024",
		"ecl:brn pid:760753108 byr:1931",
		"hgt:179cm",
		"",
		"hcl:#cfa07d eyr:2025 pid:166559648",
		"iyr:2011 ecl:brn hgt:59in"}
	res := parsePassports(input)
	output := []string{"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm",
		"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929",
		"hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm",
		"hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in"}
	require.Equal(t, output, res)
}

func Test_validPassport_True(t *testing.T) {
	res := validPassport("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm")
	require.True(t, res)
}

func Test_validPassport_False(t *testing.T) {
	res := validPassport("iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929")
	require.False(t, res)
}

func Test_countValidPassports(t *testing.T) {
	passports := []string{"ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm",
		"iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929",
		"hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm",
		"hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in"}
	res := countValidPassports(passports)
	require.Equal(t, 2, res)
}

func Test_countValidPassports_puzzle1(t *testing.T) {
	raw, err := fileToStringSlice("inputs/day4.txt")
	require.Nil(t, err)
	passports := parsePassports(raw)
	res := countValidPassports(passports)
	require.Equal(t, 170, res)
}

type fieldValidTest struct {
	fieldType      string
	fieldValue     string
	expectedResult bool
}

func Test_FieldValidator(t *testing.T) {
	testTable := []fieldValidTest{
		{fieldType: "byr", fieldValue: "1919", expectedResult: false},
		{"byr", "1920", true},
		{"byr", "2000", true},
		{"byr", "2002", true},
		{"byr", "2003", false},
		{"byr", "fdsfdsfds", false},
		{fieldType: "iyr", fieldValue: "2009", expectedResult: false},
		{"iyr", "2010", true},
		{"iyr", "2012", true},
		{"iyr", "2020", true},
		{"iyr", "2021", false},
		{"iyr", "fdsfdsfds", false},
		{fieldType: "eyr", fieldValue: "2019", expectedResult: false},
		{"eyr", "2020", true},
		{"eyr", "2025", true},
		{"eyr", "2030", true},
		{"eyr", "2031", false},
		{"eyr", "fdsfdsfds", false},
		{fieldType: "hgt", fieldValue: "149cm", expectedResult: false},
		{"hgt", "150cm", true},
		{"hgt", "170cm", true},
		{"hgt", "193cm", true},
		{"hgt", "194cm", false},
		{"hgt", "58in", false},
		{"hgt", "59in", true},
		{"hgt", "62in", true},
		{"hgt", "76in", true},
		{"hgt", "77in", false},
		{"hgt", "fdsfdsfds", false},
		{fieldType: "hcl", fieldValue: "#012345", expectedResult: true},
		{"hcl", "#6789ab", true},
		{"hcl", "#cdef00", true},
		{"hcl", "#gggggg", false},
		{"hcl", "0012345", false},
		{"hcl", "#0123450", false},
		{"hcl", "fdsfdsfds", false},
		{fieldType: "ecl", fieldValue: "amb", expectedResult: true},
		{"ecl", "blu", true},
		{"ecl", "brn", true},
		{"ecl", "gry", true},
		{"ecl", "grn", true},
		{"ecl", "hzl", true},
		{"ecl", "oth", true},
		{"ecl", "other", false},
		{"ecl", "fdsfdsfds", false},
		{fieldType: "pid", fieldValue: "012345678", expectedResult: true},
		{"pid", "000000000", true},
		{"pid", "0123454543432432", false},
		{"pid", "0000000s0", false},
		{fieldType: "cid", fieldValue: "fdsfdsfds", expectedResult: true},
		{fieldType: "", fieldValue: "", expectedResult: false},
	}
	for _, test := range testTable {
		t.Logf(`Testing fieldValid("%s", "%s")`, test.fieldType, test.fieldValue)
		res := fieldValid(test.fieldType, test.fieldValue)
		require.Equal(t, test.expectedResult, res)
	}
}
func Test_countValidPassports_puzzle2(t *testing.T) {
	raw, err := fileToStringSlice("inputs/day4.txt")
	require.Nil(t, err)
	passports := parsePassports(raw)
	res := countValidPassports2(passports)
	require.Equal(t, 103, res)
}
